#!/usr/bin/env python3
#_*_ coding: utf-8 _*_

import os
import argparse
import re


def strextract():
    parser = argparse.ArgumentParser()
    parser.add_argument('dir', help="directory path")
    parser.add_argument('--suffix', help="limite la recherche aux fichiers de ce suffixe", type = str)
    parser.add_argument('--path', help="donne le chemin de chaque chaine littérale suivi d'une tabulation", action = "store_true")
    parser.add_argument('--all', help="cherche les fichiers cachés", action = "store_true")
    pattern = re.compile(r'\'(.*)\'|\"(.*)\"')
    args = parser.parse_args()

    for root, _, filenames in os.walk(args.dir):
        for filename in filenames:
            if filename.startswith('.') and not args.all:
                continue
            if args.suffix and not filename.endswith(args.suffix):
                continue
            pathname = os.path.join(root, filename)
            try:
                with open(pathname) as file:
                    for line in file:
                        for match in pattern.finditer(line):                        
                            if args.path:
                                print(pathname, match.group(0), sep='\t')
                            else:
                                print(match.group(0))
            except:
                pass
            
if __name__ == "__main__":
    strextract()